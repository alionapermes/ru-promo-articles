# На этой неделе в KDE: the Analog Clock changes color

Because that’s clearly the most important thing this week, right!?

> 5–12 июня, основное — прим. переводчика

<!-- Поменять с учётом того, что выпуск состоялся -->

Anyway, Plasma 5.25 is going to be released in a few days, and it’ll be huge! Accordingly, feature work for 5.26 is starting to land alongside bugfixing for 5.25.

## Исправленные «15-минутные ошибки»

Current number of bugs: **64, down from 65**. 2 found to already be fixed (thanks to whoever fixed them!) and 1 added.

[Current list of bugs](https://tinyurl.com/plasma-15-minute-bugs)

## Новые возможности

Plasma [now supports wallpapers with different images displayed when using a light color scheme vs a dark color scheme](https://bugs.kde.org/show_bug.cgi?id=207976)! Expect future Plasma releases to ship both light and dark versions of our wallpapers (Fushan Wen, Plasma 5.26):

<https://i.imgur.com/sA17uVc.mp4?_=1>

## Улучшения пользовательского интерфейса

Ark [now checks to make sure that there will be sufficient free space in the place when you’re trying to un-archive something before it starts](https://bugs.kde.org/show_bug.cgi?id=393959) (Tomaz Canabrava, Ark 22.08)

When you search in KRunner, Kickoff, Overview, or any other KRunner-powered search field, [any System Settings pages that match are no longer displayed so far down in the list](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1811) (Alexander Lohnau, Plasma 5.25):

![1](https://pointieststick.files.wordpress.com/2022/06/mouse-on-top.png)

You can now drag windows between screens in the [Overview](https://bugs.kde.org/show_bug.cgi?id=448566) and [Present Windows](https://bugs.kde.org/show_bug.cgi?id=283333) effects (Marco Martin, Plasma 5.25)

Scrolling over the Media Controller widget’s icon [now changes the volume of the app playing media in steps of 5%, not 3%](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1810), so now it matches the default step size when changing the whole system volume. Also like the system volume, the step size is configurable! (Oliver Beard, Plasma 5.26)

Breeze-styled buttons [no longer have a gradient on them when not being hovered by the cursor](https://invent.kde.org/plasma/breeze/-/merge_requests/220#note_465153), which effectively makes them look a little bit lighter and stand out more from the page background (Someone who wishes to remain anonymous, Plasma 5.26):

![2](https://pointieststick.files.wordpress.com/2022/06/lighter-flatter-buttons.png)

The common «Keyboard Shortcuts» dialog you’ll see in many apps and System Settings [no longer shows you empty «Global Shortcuts» columns when the app doesn’t set any global shortcuts, or empty «Local shortcuts» columns when it only sets global shortcuts](https://bugs.kde.org/show_bug.cgi?id=427129) (Ahmad Samir, Frameworks 5.95):

![3](https://pointieststick.files.wordpress.com/2022/06/elisa-shortcut-dialog.png)

The tickmarks and digits on the Analog Clock [now respect your accent color](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/543) (Ismael Asensio, Frameworks 5.95):

![4](https://pointieststick.files.wordpress.com/2022/06/volna-accented-clock.png)

…And in addition, [the entire Analog Clock’s face also respects your color scheme too!](https://bugs.kde.org/show_bug.cgi?id=377935) (Ismael Asensio, Frameworks 5.96):

![5](https://pointieststick.files.wordpress.com/2022/06/black-clock.png)

The animated transition when the wallpaper changes from one image to another [now respects the global animation duration setting](https://invent.kde.org/plasma/plasma-workspace/-/commit/8df97ad32394500af9e1195048e5121a1b9c9adc) (Fushan Wen, Plasma 5.26)

Throughout QtQuick-based software, views where one thing transition into another thing [now respect the global animation duration setting](https://bugs.kde.org/show_bug.cgi?id=395324) (Fushan Wen, Frameworks 5.96)

## Исправления ошибок и улучшения производительности

Thumbnail previews for a folder’s contents [are once again generated as expected, rather than only after you have navigated to the folder](https://bugs.kde.org/show_bug.cgi?id=453497) (Martin T. H. Sandsmark, Dolphin 22.08)

Automount [is once again disabled by default as expected](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/974) (Ismael Asensio, Plasma 5.25)

SDDM login screen themes downloaded using the «Get New [thing]» downloader window [now appear in the System Settings Login Screen page immediately, rather than only after it is closed and re-opened](https://bugs.kde.org/show_bug.cgi?id=454884) (Alexander Lohnau, Plasma 5.24.6)

Automatic Touch Mode detection [now ignores fake input devices, so it can no longer become blocked when an app that creates any such fake input devices is running](https://invent.kde.org/plasma/kwin/-/merge_requests/2494) (Александр Волков, Plasma 5.24.6)

Repeatedly mounting and unmounting a disk [no longer sometimes causes its list of actions in the «Disks & Devices» widget to become ever longer and accumulate blank entries](https://bugs.kde.org/show_bug.cgi?id=449778) (Иван Ткаченко, Plasma 5.25)

It’s [no longer possible to undo deleting text in Plasma’s Password input fields](https://bugs.kde.org/show_bug.cgi?id=453828), which increases security a bit (Derek Christ, Frameworks 5.95 and Plasma 5.26)

When you use the «Go Up» action in Dolphin, [the folder you just came from is once again highlighted](https://bugs.kde.org/show_bug.cgi?id=453289) (Jan Blackquill, Frameworks 5.95)

The shortcut dialog [once again shows you accurate information about shortcut conflicts](https://bugs.kde.org/show_bug.cgi?id=454704) (Ahmad Samir, Frameworks 5.96)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Тимофей Москвин](https://t.me/alionapermes/)  
*Источник:* <https://pointieststick.com/2022/06/10/this-week-in-kde-the-analog-clock-changes-color/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: thumbnail → миниатюра -->
